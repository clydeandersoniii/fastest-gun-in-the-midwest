﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;

public class InputHandler : MonoBehaviour
{
    public Text winner;
    public UnityEvent playerOneHit;
    public UnityEvent playerTwoHit;

    // Start is called before the first frame update
    void Start()
    {
        //Debug.Log("Start");
        winner.text = "Start";
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown("a"))
        {
            //Player 1 gets the A button
            //Debug.Log("Player 1 wins!");
            //winner.text = "Player 1 wins!";
            playerOneHit.Invoke();
        }
        else if (Input.GetKeyDown("l"))
        {
            //Player 2 gets the B button
            //Debug.Log("Player 2 wins!");
            //winner.text = "Player 2 wins!";
            playerTwoHit.Invoke();
        }
    }
}