﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Threading;
using UnityEngine.Events;

public class Countdown : MonoBehaviour
{

    public int timeLeft = 5;
    public Text countDown;
    public Text numPressesP1;
    public Text numPressesP2;
    public bool flag = false;
<<<<<<< HEAD
    public bool displayStop = false;
=======
    public UnityEvent countdownFinished;
>>>>>>> determining_winner_clyde

    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine("LoseTime");
        Time.timeScale = 1;
        countDown.text = "";

        //make text fields transparent
        //Color color = numPresses.color;
        //color.a = 1.0f;
        //numPresses.material.color = color;
    }

    // Update is called once per frame
    void Update()
    {
        countDown.text = ("COUNT DOWN: " + timeLeft);
        if (countDown.text == "COUNT DOWN: 0")
        {
            countDown.text = "FIRE!";
            if (!displayStop)
            {
                displayPresses();
                displayStop = true;
            }
        }
    }

    public void displayPresses()
    {
        int ran = Random.Range(0, 4);
        //Color color = numPresses.color;
        //color.a = 0.0f;
        //numPresses.material.color = color;
        if (ran == 0)
        {
            numPressesP1.text = "DON'T FIRE!!!";
            numPressesP2.text = "DON'T FIRE!!!";
        }
        else
        {
            string temp = "" + ran;
            numPressesP1.text = temp;
            numPressesP2.text = temp;
        }
    }

    IEnumerator LoseTime()
    {
        while (!flag)
        {
            yield return new WaitForSeconds(1);
            timeLeft--;
            if (timeLeft == 0)
            {
                flag = true;
            }
        }

        countdownFinished.Invoke();
    }


}
