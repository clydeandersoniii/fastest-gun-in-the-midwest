﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class GameState : MonoBehaviour
{
    public bool countdownFinished = false;
    public UnityEvent playerOneWin;
    public UnityEvent playerTwoWin;
    public UnityEvent playerShotEarly;
    public UnityEvent validShot;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void PlayerOneShot()
    {
        if (!countdownFinished)
        {
            playerTwoWin.Invoke();
            playerShotEarly.Invoke();
        }
        else
        {
            playerOneWin.Invoke();
            validShot.Invoke();
        }

    }

    public void PlayerTwoShot()
    {
        if (!countdownFinished)
        {
            playerOneWin.Invoke();
            playerShotEarly.Invoke();
        }
        else
        {
            playerTwoWin.Invoke();
            validShot.Invoke();
        }

    }

    public void SetGameState()
    {
        countdownFinished = true;
    }
}
